<?php

/**
 * @file
 * Helper functions for installation
 */

/**
 * Install a cck type from a file.
 */
function _club_install_cck($path, $name) {
  $filename = $name . '.cck';

  $form_state['values']['type_name'] = '<create>';
  $form_state['values']['macro'] = file_get_contents($path . '/' . $filename);

  module_enable(array('content_copy'));
  drupal_execute("content_copy_import_form", $form_state);
}

/**
 * Disable the post information for the specified node type.
 */
function _club_disable_post_information($type) {
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_' . $type] = FALSE;
  variable_set('theme_settings', $theme_settings);
}

/**
 * Set the pathauto pattern for a node type.
 */
function _club_set_pathauto_pattern_for_type($type, $path_pattern) {
  variable_set('pathauto_node_'. $type .'_pattern', $path_pattern);
}

/**
 * Create an imagecache preset.
 */
function _club_create_imagecache_preset($name, $width, $height) {
  $presetid = db_result(db_query("SELECT presetid FROM {imagecache_preset} WHERE presetname='%s'", $name));
  if (!$presetid) {
    db_query("INSERT INTO {imagecache_preset} (presetname) VALUES ('%s')", $name);
    $presetid = db_result(db_query("SELECT presetid FROM {imagecache_preset} WHERE presetname='%s'", $name));
    db_query("INSERT INTO {imagecache_action} (presetid, action, data) VALUES (%d, '%s', '%s')",
      $presetid,
      'imagecache_scale', 
      serialize(array(
          'width' => $width,
          'height' => $height,
          'upscale' => 0,
        )
      )
    );
  }
  imagecache_presets(TRUE);
}

/**
 * Import a node using the 'node_export' module.
 */
function _club_import_node($path, $name) {
  require_once(drupal_get_path('module', 'node_export') . '/node_export.pages.inc');
	$filename = $name . '.node';
  $file_content = file_get_contents($path . '/' . $filename);
  $import = node_export_node_decode($file_content);
  if (!is_array($import)) {
  	$nodes = array($import);
  }
  else {
  	$nodes = $import;
  }
  foreach ($nodes as $node) {
    node_export_node_save($node);
  }
}

/**
 * Grant permissions for anynomous user.
 */
function _club_grant_anonymous($permissions) {
  // TODO: use of a defined constant would be nice
	_club_grant_permissions(1, $permissions);
}

/**
 * Grant permissions for authenticated user.
 */
function _club_grant_authenticated($permissions) {
  // TODO: use of a defined constant would be nice
  _club_grant_permissions(2, $permissions);
}

/**
 * Grant permissions for editor.
 */
function _club_grant_editor($permissions) {
	// TODO: use of a defined constant would be nice
  _club_grant_permissions(3, $permissions);
}

/**
 * Grant permissions for administrator.
 */
function _club_grant_administrator($permissions) {
  // TODO: use of a defined constant would be nice
  _club_grant_permissions(4, $permissions);
}

/**
 * Grant permissions.
 */
function _club_grant_permissions($role_id, $permissions) {
  $result = db_query("SELECT p.perm FROM {role} r JOIN {permission} p ON r.rid=p.rid WHERE r.rid = %d", $role_id);
  $previous_permissions = array();
  while ($row = db_fetch_object($result)) {
    $previous_permissions += explode(', ', $row->perm);
  }
  $permissions = array_unique(array_merge($previous_permissions, $permissions));
  
  db_query('DELETE FROM {permission} WHERE rid = %d', $role_id);
  db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", $role_id, implode(', ', $permissions));
}




